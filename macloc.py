import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt
import argparse, sys
from geopy.geocoders import Nominatim
import json
import datetime



#preparing variables
wigle_username = ''
wigle_password = ''
latitudelist = []
longitudelist = []
#configuration file
conf = "config.json"
# define the program description
progdesc = "Get average geolocation from a list of AP mac addresses"
#geolocator used for reverse search
geolocator = Nominatim(user_agent="macloc",timeout=None)


#getting the wigle api key from config file
try:
	with open(conf, 'r') as f:
	    array = json.load(f)
	    wigle_username = array['wigle_username']
	    wigle_password = array['wigle_password']
	    f.close()
except IOError:
	print("No config.json found. please create one based on config.json.dist")
	exit()
except json.decoder.JSONDecodeError:
	print("The config.json file is not properly formatted. please create one based on config.json.dist")
	exit()


# initiate the cli argument parser
parser = argparse.ArgumentParser(description = progdesc)
parser.add_argument('-o','--output', help='choice of output', choices=['address', 'coordinates', 'all'],type=str)
parser.add_argument('-v','--verbose', help='print verbos logs', action='store_true')
parser.add_argument('-i','--input', help='mac address list file', required=True, type=str)
parser.add_argument('-l','--language', help='set address language',type=str)
args = parser.parse_args()
parser.set_defaults(verbose=False, language='fr-fr')
inputfile = args.input
verbose = args.verbose
output = args.output
lang = args.language



with open(inputfile, "r") as fd:
	for line in fd:
		macaddress = line.strip()
		# payload sent to wigle for querying
		payload = {'netid': macaddress, 'api_key': (wigle_username + wigle_password).encode()}

		# FETCHING JSON RESPONSE FROM WiGLE:
		result_details = requests.get(url='https://api.wigle.net/api/v2/network/search', params=payload, auth=HTTPBasicAuth(wigle_username, wigle_password)).json()

		status = result_details["success"]
		#If we get a successful query
		if status == True :
			if result_details["totalResults"] == 0:
				#No result found
				if verbose :
					print("[-] {} No match found".format(macaddress))
			elif result_details["totalResults"] > 1:
				#Found more than one result for the same mac address
				if verbose :
					print("[+] {} YAY We got {} hits".format(macaddress, result_details["totalResults"]))
				templat = ''
				templong = ''
				#initiate a former time that is in 1970 so the last date from wigle is always more recent
				formertime = datetime.datetime.strptime("1970-01-01T01:00:00.000Z", "%Y-%m-%dT%H:%M:%S.%f%z")
				for ap in result_details["results"]:
					latitude = ap["trilat"]
					longitude = ap["trilong"]
					last = ap["lasttime"]
					lasttime = datetime.datetime.strptime(ap["lasttime"], "%Y-%m-%dT%H:%M:%S.%f%z")

					#add the long & lat to arrays so we can create an estimate
					#TODO : only add the last seen coordinates instead of all of them.
					if lasttime > formertime :
						#this is meant to only keep the coordinates of the latest record
						formertime = lasttime
						templong = longitude
						templat = latitude
				if verbose :
					print("the latest result found was {},{} on {}".format(templat,templong,formertime))
				latitudelist.append(templat)
				longitudelist.append(templong)	

			else :
				#found one relsult for this mac address
				if verbose :
					print("[+] {} : YAY, we found 1 hit".format(macaddress))
				housenumber = result_details["results"][0]["housenumber"]
				road = result_details["results"][0]["road"]
				postalcode = result_details["results"][0]["postalcode"]
				city = result_details["results"][0]["city"]
				region = result_details["results"][0]["region"]
				country = result_details["results"][0]["country"]
				#add the long & lat to arrays so we can create an estimate

				latitudelist.append(result_details["results"][0]["trilat"])
				longitudelist.append(result_details["results"][0]["trilong"])	

				#print out individual result if verbose mode is on
				if verbose :
					if output == "coordinates" :
						print("[+] {},{}".format(result_details["results"][0]["trilat"], result_details["results"][0]["trilong"]))		
					elif output == "address" :
						print("[+] {} {}, {} {} / {} / {}".format(housenumber, road, postalcode, city, region, country))		
					else :
						print("[+] {},{} : {} {}, {} {} / {} / {}".format(result_details["results"][0]["trilat"], result_details["results"][0]["trilong"],housenumber, road, postalcode, city, region, country))		
		else : 
			#most likely, daily rate limit reached
			print("[-] {} Error querying wigle {}".format(macaddress,result_details))
			exit(2)

if len(latitudelist) > 0 :
	#calculate average coordinates
	avglat=sum(latitudelist)/len(latitudelist)
	avglon=sum(longitudelist)/len(longitudelist)
	#get address from average coordinate
	estimated_address = geolocator.reverse([avglat,avglon],True,5,lang,True)
	
	#print out the result
	if output == "coordinates" :
		print("[==] estimated position : {},{}".format(avglat,avglon))
	elif output == "address" :
		print("[==] estimated address : {} ".format(estimated_address))
	else : 
		print("[==] result : {},{} : {}".format(avglat,avglon,estimated_address))

else : 
	#If no result has been fetched from wigle, cannot estimate location
	print("[xx] No position could be established")