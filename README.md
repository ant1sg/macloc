# macloc

Python3 script used to calculate the most likely location of a device based on a list of nearby wifi AP mac addresses.

This script requires python 3.x

## Installation
Install the Requirements 
pip install -r requirements.txt

## APIs
This script uses the wigle.net api to get the access point's geolocation so you will need to create an account there.
There is a daily rate limit on wigle.net, do not stress it too much

This script uses also the openstreetmaps nominatim api to run reverese search on gps coordinates in order to get the estimated street location from the calculated gps coordinates.

## Usage
You need to have a text file with all the mac addresses as a list.
If required you can use the shell script I included to generate this file (grabnetid.sh)
All you have to do is scan the surrounding wifi accesspoints and grab their IP addresses.

The MacLoc script itself takes a few arguments
+ *verbose* : adds some logging
+ *input* : path & name of the file containing the mac addresses
+ *output* : the kind of output you wish to have :
	+ *address* : plain text human readable street address
	+ *coordinates* : gps coordinates
	+ *all* : well, pretty self explanatory isn't it?
+ *language* : the language use to generate the street addresses

Example usage : 
`python3 macloc.py -i input.txt --output address -v`